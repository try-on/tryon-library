<?php

namespace Library\Controller;

use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractRestfulController;

/**
 * Class BaseRestfulController
 * @package Library\Controller
 */
class BaseRestfulController extends AbstractRestfulController implements EventManagerAwareInterface {

    protected $_allowedResourceMethods = array('GET', 'PUT', 'OPTIONS');
    protected $_allowedCollectionMethods = array('GET', 'POST', 'DELETE', 'OPTIONS');

    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);
        $events->attach('dispatch', array($this, 'checkOptions'), 10);
    }

    public function options() {
        $response = $this->getResponse();
        $headers  = $response->getHeaders();

        $response->getHeaders()->addHeaderLine('Access-Control-Allow-Origin', '*');
        // If you want to vary based on whether this is a collection or an
        // individual item in that collection, check if an identifier from
        // the route is present
        if($this->params()->fromRoute('id', false)) {
            $headers->addHeaderLine('Access-Control-Allow-Methods', implode(',', $this->_allowedResourceMethods));
            return $response;
        }
        // Allow only retrieval and creation on collections
        $headers->addHeaderLine('Access-Control-Allow-Methods', implode(',', $this->_allowedCollectionMethods));
        return $response;
    }

    public function checkOptions($e) {
        $matches  = $e->getRouteMatch();
        $response = $e->getResponse();
        $request  = $e->getRequest();
        $method   = $request->getMethod();
        $response->getHeaders()->addHeaderLine('Access-Control-Allow-Origin', '*');

        // test if we matched an individual resource, and then test
        // if we allow the particular request method
        if($matches->getParam('id', false)) {
            if (!in_array($method, $this->_allowedResourceMethods)) {
                $response->setStatusCode(405);
                return $response;
            }
            return;
        }

        // We matched a collection; test if we allow the particular request
        // method
        if (!in_array($method, $this->_allowedCollectionMethods)) {
            $response->setStatusCode(405);
            return $response;
        }
    }

    /**
     * get base url
     * @return string
     */
    protected function getBaseUri() {
        $uri = $this->getRequest()->getUri();
        return sprintf('%s://%s', $uri->getScheme(), $uri->getHost());
    }

}