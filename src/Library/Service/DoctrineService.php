<?php

namespace Library\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Library\Entity\AbstractEntity;

class DoctrineService
{

    private $_entityManager;
    protected $_entityName;

    public function __construct(EntityManager $entityManager)
    {
        $this->_entityManager = $entityManager;
    }

    /**
     * Find entities by a set of criteria.
     *
     * @param array $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getRepository()->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * Find a single entity by a set of criteria.
     *
     * @param array $criteria
     * @param array $orderBy
     * @return object
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->getRepository()->findOneBy($criteria, $orderBy);
    }

    /**
     * @param int $id
     * @return AbstractEntity
     */
    public function fetch($id)
    {
        return $this->getRepository()->find($id);
    }

    /**
     * @param array $options
     * @return array
     */
    public function fetchAll(array $options = null)
    {

        if (is_array($options)) {

        } else
            return $this->getRepository()->findAll();
        return array();
    }

    /**
     * @param AbstractEntity $entity
     */
    public function create(AbstractEntity $entity)
    {
        $this->getEntityManager()->persist($entity);
    }

    /**
     * @param AbstractEntity $entity
     */
    public function delete(AbstractEntity $entity)
    {
        $this->getEntityManager()->remove($entity);
    }

    public function paginate($start, $max) {
        $query = $this->getRepository()->createQueryBuilder('d')->setFirstResult($start * $max)->setMaxResults($max);
        return new Paginator($query);
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->_entityManager;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     * @throws \Exception
     */
    protected function getRepository()
    {
        if (!$this->_entityName)
            throw new \Exception("You have to specify the entities name");
        return $this->getEntityManager()->getRepository($this->_entityName);
    }
}