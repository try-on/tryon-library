<?php

namespace Library\Listener;

use Zend\ServiceManager\ServiceLocatorInterface;

class InjectListener {
    private $_serviceLocator;

    public function __construct(ServiceLocatorInterface $serviceLocator) {
        $this->_serviceLocator = $serviceLocator;
    }

    public function postLoad($eventArgs) {
        /** @var BaseEntity $entity */
        $entity = $eventArgs->getEntity();
        $entity->setServiceLocator($this->_serviceLocator);
    }
} 