<?php

namespace Library\Entity;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractEntity
 * @package Application\Entity
 */
abstract class AbstractEntity implements \JsonSerializable, ServiceLocatorAwareInterface
{

    private $_serviceLocator;
    private $_properties;
    private $_methods;
    private $_modelName;

    /**
     * @ORM\Column(type="datetime")
     * @var \Datetime
     */
    public $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @var \Datetime
     */
    public $updatedAt;

    /**
     * takes an array with "key" => "value" pairs as optional parameter
     * @param array $options
     * @throws \Exception
     */
    public function __construct(array $options = null)
    {
        if (is_array($options))
            $this->setOptions($options);
    }

    /**
     * @param array $options
     * @return $this
     * @throws \Exception
     */
    public function setOptions(array $options)
    {
        $methods = $this->getMethods();
        $properties = $this->getProperties();
        foreach ($options as $key => $value) {
            if ($value === "")
                $value = null;
            $method = "set" . ucfirst($key);
            if(in_array($method, $methods)) {
                $this->$method($value);
            } else if(array_search($key, $properties) !== FALSE) {
                $this->{$key} = $value;
            } else {
                throw new \Exception($method . " could not be called.");
            }
        }
        return $this;
    }

    /**
     * ensure that if specified, the setter is called
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $method = 'set' . ucfirst($name);
        if (!method_exists($this, $method)) {
            $vars = $this->getProperties();
            if (array_search($name, $vars) !== FALSE)
                $this->{$name} = $value;
        } else
            $this->$method($value);
    }

    /**
     * ensure that, if specified, the getter is called
     * @param $name
     * @return null
     */
    public function __get($name)
    {
        $method = 'get' . ucfirst($name);
        if (!method_exists($this, $method)) {
            $vars = $this->getProperties();
            if (array_search($name, $vars) !== FALSE)
                return $this->{$name};
        } else
            return $this->$method();
        return null;
    }

    /**
     * get all protected|public properties of this object
     * @return array
     */
    protected function getProperties()
    {
        if (!$this->_properties) {
            $reflect = new \ReflectionClass($this);
            $this->_properties = array();
            foreach ($reflect->getProperties(\ReflectionProperty::IS_PUBLIC) as $var) {
                if (substr($var->name, 0, 2) != '__')
                    $this->_properties[] = $var->name;
            }
        }
        return $this->_properties;
    }

    /**
     * @return array
     */
    protected function getMethods()
    {
        if (!$this->_methods)
            $this->_methods = get_class_methods($this);
        return $this->_methods;
    }

    /**
     * returns all public properties and their values as an array
     * @return array
     */
    public function toArray()
    {
        $data = array();
        foreach ($this->getProperties() as $property) {
            $methods = get_class_methods($this);
            $method = 'get' . ucfirst($property);
            if (in_array($method, $methods))
                $value = $this->$method();
            else
                $value = $this->{$property};
            $data[$property] = $value == "" ? NULL : $value;
        }
        return $data;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * required method, so the servicelocator will be injected on creation
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->_serviceLocator = $serviceLocator;
    }

    /**
     * @return ServiceLocatorInterface
     * @throws \Exception
     */
    public function getServiceLocator()
    {
        if (!$this->_serviceLocator)
            throw new \Exception("ServiceLocator was not injected.");
        return $this->_serviceLocator;
    }

    /**
     * helper method, to get the modelname of existing model
     * e.g. application_cloth
     * @return mixed|null
     */
    public function getModelName()
    {
        if (!$this->_modelName) {
            $this->_modelName = str_replace("_entity", "", str_replace("\\", "_", strtolower(get_class($this))));
        }
        return $this->_modelName;
    }

    /**
     * @ORM\PrePersist
     */
    public function setTimestamps()
    {
        if (!$this->createdAt)
            $this->createdAt = new \DateTime(date('Y-m-d H:i:s'));
        $this->updatedAt = new \DateTime(date('Y-m-d H:i:s'));
    }
} 